#!flask/bin/python
from flask import Flask, jsonify, request, abort, render_template
from utils import update_stdict, create_suggestions

app = Flask(__name__)


@app.route('/')
def index():
    return render_template("home.html")


@app.route('/form')
def form():
    return render_template('form.html')


@app.route('/data/', methods=['POST', 'GET'])
def data():
    if request.method == 'GET':
        return f"The URL /data is accessed directly. Try going to '/form' to submit form"
    if request.method == 'POST':
        form_data = request.form
        return render_template('data.html', form_data=form_data)


@app.route('/get_suggestions', methods=['POST'])
def get_suggestions():
    inpt = request.json
    suggestions = create_suggestions(
        inpt['stdict'],
        inpt['stext'], inpt['slemmadict'], inpt['sstopwords']
    )
    return jsonify(suggestions)


@app.route('/update_stdict', methods=['POST'])
def get_updated_stdict():
    inpt = request.json
    updated_stdict = update_stdict(
        inpt['stdict'],
        inpt['stext'], inpt['slemmadict'], inpt['sstopwords'],
        inpt['ttext'], inpt['tlemmadict'], inpt['tstopwords']
    )
    return jsonify(updated_stdict)


if __name__ == '__main__':
    app.run(debug=True)


