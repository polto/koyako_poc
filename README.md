## Basic principle

This is to explore a simple idea: when translating a text, repeating words should be translated consistently, i.e. "kočka" should (almost) always be translated as "cat" - we can exploit this to deduce translations of the words without a dictionary and without user specifically providing translation of the word. Let's consider these two sentences and their translations:

- "Toto je kočka." = "This is a cat."
- "Kočka má tři životy." = "A cat has three lives."

Even without knowing any Czech, we can see that the only word occuring in both is "kočka", so it's very likely that "cat" = "kočka".

![demo](demo/demo.svg)

## Installation

- `pipenv install` to install required dependencies specified in Pipfile
- download *Morphodita* from https://github.com/ufal/morphodita/releases, unzip into *tools* folder
- download language model, e.g. https://github.com/kasev/czech_textual_networks/blob/master/czech-morfflex-pdt-161115.tagger, save into *tools/czech-morfflex-pdt-161115*

## Usage

- in one terminal: `pipenv shell` to activate the environment and `python app.py` as the server side
- in another terminal: `pipenv shell` to activate the environment and `python interactive.py` as the client side

## Solution design

- app.py is running on server, accepting two types of json requests: 1. get_suggestions() and 2. updated_stdict()
- interactive.py takes a local docx file, splits it and provides to users for translation, along with suggestions


