import re
import docx
import json
import requests
import utils
import config

cfg = config.Config('config.yml')

path2docx = cfg['path2docx']
doc = docx.Document(path2docx)

with open("data/czech_stopwords.txt") as f:
    sstopwords = f.read().splitlines()

stdict = utils.load_dict("data/slovnik_split_cz-en.json")
sextracted = utils.doc2text(doc)
snormalized = utils.normalize_text(' '.join(sextracted), sstopwords)
slemmadict = utils.create_lemma_dict(' '.join(snormalized))

with open("data/english_stopwords.txt") as f:
    tstopwords = f.read().splitlines()
tlemmadict = {}


par_index = 0
while par_index < len(doc.paragraphs):
    par = doc.paragraphs[par_index]
    if par.text.strip() == '':
        par_index += 1
        continue
    print(f"----------------------------------------------------------------> par {par_index}")
    stext = par.text
    rqst_suggestions = {
        'stdict': stdict,
        'stext': stext, 'slemmadict': slemmadict, 'sstopwords': sstopwords
    }
    response_suggestions = requests.post(url="http://127.0.0.1:5000/get_suggestions", json=rqst_suggestions)
    for key, value in json.loads(response_suggestions.text).items():
        print(f"{key}: {utils.sort_dictionary(value)}")
    print(par.text)
    ttext = input()

    # writing "xxx" as input translation for a segment will terminate session and write output into file
    if ttext == "xxx":
        doc.save(re.sub("\.docx", "_trans.docx", path2docx))
        break

    # writing "xx[number]" will cause the program to jump into paragraph specified after the "xx"
    jump = re.match(r"xx[0-9]*", ttext)
    if jump:
        par_index = int(re.sub(r'xx', '', jump.group(0)))
        continue
    doc.paragraphs[par_index].text = ttext

    # only now can the dictionary be updated (the important part is translation from user ttext)
    rqst_update_dict = {
        'stdict': stdict,
        'stext': stext, 'slemmadict': slemmadict, 'sstopwords': sstopwords,
        'ttext': ttext, 'tlemmadict': tlemmadict, 'tstopwords': tstopwords
    }

    stdict_string = requests.post(url="http://127.0.0.1:5000/update_stdict", json=rqst_update_dict).text
    stdict = json.loads(stdict_string)
    par_index += 1

doc.save(re.sub("\.docx", "_translated.docx", path2docx))

with open(re.sub("\.docx", "_dict.json", path2docx), "w") as f:
    json.dump(stdict, f, indent=6)

