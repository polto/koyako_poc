import pytest
from utils import update_stdict


@pytest.fixture
def small_stdict():
    return {'kočka': {'cat': 1, 'dog': 1}, 'pes': {'dog': 5, 'mouse': 2}}


@pytest.fixture
def small_slemmadict():
    return {'toto': ['toto'], 'je': ['je'], 'kočka': ['kočka', 'kočky']}


@pytest.fixture
def small_tlemmadict():
    return {'this': ['this'], 'is': ['is'], 'cat': ['cat', 'cats']}


def test_update_stdict(small_stdict, small_slemmadict, small_tlemmadict):
    updated_stdict = update_stdict(
        small_stdict,
        'toto je kočky', small_slemmadict, [],
        'this is cats', small_tlemmadict, []
    )
    assert updated_stdict['kočka']['cat'] == 2


