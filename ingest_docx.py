
import re
import json
import docx


def docx2json(path2docx):
    doc = docx.Document(path2docx)
    x = {'paragraphs': []}
    for par_id in range(len(doc.paragraphs)):
        for run_id in range(len(doc.paragraphs[par_id].runs)):
            extracted_text = doc.paragraphs[par_id].runs[run_id].text
            if re.search('[a-zA-Z]', extracted_text):
                existing_paragraphs = [par['id'] for par in x['paragraphs']]
                if par_id not in existing_paragraphs:
                    x['paragraphs'].append({'id': par_id, 'runs': []})
                x['paragraphs'][len(x['paragraphs'])-1]['runs'].append({'id': run_id, 'text': extracted_text})
    return x


def save2json(x, path2docx):
    with open(path2docx.replace('.docx', '.json'), 'w') as f:
        json.dump(x, f)


def load_from_json(path2json):
    with open(path2json, 'r') as f:
        x = json.load(f)
    return x


def json2docx(x, path2docx):
    """
    Takes original docx file and dict{list[dict]} containing paragraphs and runs and inserts them into locations
    corresponding to the par and run ids.
    :param x: a composite dictionary containing text to be inserted into docx
    :param path2docx: parth to the original docx document
    :return:
    """
    path2docx_changed = path2docx.replace('.docx', '_changed.docx')
    doc = docx.Document(path2docx)
    for par in x['paragraphs']:
        for run in par['runs']:
            doc.paragraphs[par['id']].runs[run['id']].text = run['text']
    doc.save(path2docx_changed)
