'''
Launch app.py as flask app, then run this script.
The expected output is an updated stdict, where "kocka":{"cat":2,...} and "pes":{"dog":5,...}.
'''
import requests

response = requests.get("http://127.0.0.1:5000")
print(response.text)

stdict = {'kocka': {'cat': 1, 'dog': 1}, 'pes': {'dog': 5, 'mouse': 2}}
stext = 'toto je kocky'
slemmadict = {'toto': ['toto'], 'je': ['je'], 'kocka': ['kocka', 'kocky']}
sstopwords = []
ttext = 'this is cats'
tlemmadict = {'this': ['this'], 'is': ['is'], 'cat': ['cat', 'cats']}
tstopwords = []

rqst_suggestions = {
    'stdict': stdict,
    'stext': stext, 'slemmadict': slemmadict, 'sstopwords': sstopwords
}
response_suggestions = requests.post(url="http://127.0.0.1:5000/get_suggestions", json=rqst_suggestions)
print(response_suggestions.text)

rqst_update_dict = {
    'stdict': stdict,
    'stext': stext, 'slemmadict': slemmadict, 'sstopwords': sstopwords,
    'ttext': ttext, 'tlemmadict': tlemmadict, 'tstopwords': tstopwords
}

response_update_dict = requests.post(url="http://127.0.0.1:5000/update_stdict", json=rqst_update_dict)
print(response_update_dict.text)



