import os
import re
import json
import subprocess
import xml.etree.ElementTree as ET
import zipfile as zf
import config


cfg = config.Config('config.yml')


def clean_line(line: str):
    return re.sub('\n', '', line).split('.')


def load_stop_words(path2file: str):
    stop_words = open(path2file, 'r').read().split('\n')
    return stop_words


def normalize_text(txt: str, stop_words) -> list:
    txt_normalized = ''.join([char.lower() for char in txt if char.isalnum() or char == ' ']).strip()
    return [word for word in txt_normalized.split(' ') if word not in stop_words and word != '']


def update_dict_entry(current: dict, new: list):
    for word in new:
        if word in current:
            current[word] += 1
        else:
            current.update({word: 1})
    return current


def sort_dictionary(d: dict):
    d_out = {}
    for w in sorted(d, key=d.get, reverse=True):
        d_out.update({w: d[w]})
    return d_out


def lemmatize(text: str):
    path2morphodita = cfg['path2morphodita']
    path2tagger = cfg['path2tagger']
    with open('lemmatize.txt', 'w') as f:
        f.write(text.lower())
    subprocess.call([
        f'./{path2morphodita}',
        '--convert_tagset=strip_lemma_id',
        f'{path2tagger}',
        'lemmatize.txt:lemmatized.txt'
    ])
    with open('lemmatized.txt', 'r') as f:
        lemmatized = f.readlines()
    os.remove('lemmatize.txt')
    os.remove('lemmatized.txt')
    return lemmatized


def get_lemmas(text: str):
    lemmatized = lemmatize(text)
    normalized = re.findall(r'<token lemma="(.+?)" tag=', lemmatized[0])
    return normalized


def create_lemma_dict(text: str):
    lemmatized = lemmatize(text)
    lemmatized_padded = f'<root>{lemmatized[0]}</root>'
    dict = {}
    root = ET.fromstring(lemmatized_padded)
    for sentence in root:
        for child in sentence:
            if child.attrib['lemma'] == '.':
                continue
            if child.attrib['lemma'] not in dict.keys():
                dict.update({child.attrib['lemma']: [child.text]})
            else:
                if child.text not in dict[child.attrib['lemma']]:
                    dict[child.attrib['lemma']].append(child.text)
    return dict


def find_in_lemma_dict(word: str, lemma_dict: dict):
    for lemma in lemma_dict.keys():
        if word in lemma_dict[lemma]:
            return lemma
    return word


def lemmatize_using_lemma_dict(text: list, lemma_dict: dict) -> list:
    output = []
    for word in text:
        output.append(find_in_lemma_dict(word, lemma_dict))
    return list(set(output))


def create_suggestions(stdict: dict, stext: str, slemmadict: dict, sstopwords: list) -> dict:
    """
    In the stdict finds words from the source text (stext) as suggestions for the translator.
    :param stdict: dictionary from source to target language
    :param stext: original text in the source language
    :param slemmadict: lemma dictionary for the source language
    :param sstopwords: list of stop words in the source language
    :return: a dictionary of suggested translations for individual words from the source text
    """
    stext_normed = normalize_text(stext, sstopwords)
    stext_lemmas = lemmatize_using_lemma_dict(stext_normed, slemmadict)
    result = {}
    for word in stext_lemmas:
        if word in stdict:
            result.update({word: sort_dictionary(stdict[word])})
    return result


def update_stdict(stdict: dict,
                  stext: str, slemmadict: dict, sstopwords: list,
                  ttext: str, tlemmadict: dict, tstopwords: list) -> dict:
    """
    's' generally refers to 'source language'
    't' generally refers to 'target language'
    Updates the source-target dictionary (stdict) using the source text (stext) and text translated into the target
    language (ttext). The text is cleaned using lemma dictionaries and stop words.
    :param stdict: dictionary from source to target language
    :param stext: original text in the source language
    :param slemmadict: lemma dictionary for the source language
    :param sstopwords: list of stop words in the source language
    :param ttext: text translated into the target language
    :param tlemmadict: lemma dictionary for the target language
    :param tstopwords: list of stope words in the target language
    :return: updated stdict
    """
    stext_normed = normalize_text(stext, sstopwords)
    stext_lemmas = lemmatize_using_lemma_dict(stext_normed, slemmadict)
    ttext_normed = normalize_text(ttext, tstopwords)
    ttext_lemmas = lemmatize_using_lemma_dict(ttext_normed, tlemmadict)
    #alternatively:
    #ttext_lemmas = ttext_normed
    print(stext_lemmas)
    for sword in stext_lemmas:
        if sword in stdict:
            stdict[sword] = update_dict_entry(stdict[sword], ttext_lemmas)
        else:
            stdict.update({sword: {tword: 1 for tword in ttext_lemmas}})
    return stdict


def doc2text(doc):
    text = []
    for par in doc.paragraphs:
        if par.text.strip() == '':
            continue
        text.append(par.text.strip())
    return text


def ingest_docx(path2docx):
    z = zf.ZipFile(path2docx)
    f = z.open("word/document.xml")   # a file-like object
    tree = ET.parse(f)                # an ElementTree instance

    for elem in tree.iter():
        print(elem)


def load_dict(path2dict: str):
    dictionary = {}
    path2dict = "data/slovnik_split_cz-en.json"
    with open(path2dict, 'r') as f:
        dict_raw = json.load(f)
    for term, definition in dict_raw.items():
        dictionary.update({term: {x: 1 for x in definition}})
    return dictionary

